from django.template import loader
import json
import random
from django.shortcuts import render
from django.db.models import Q
from django.http  import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from pnl.models import Usuario, Aprendizaje, Temas, SubTemas, Ejercicio, TemaUsuario, EjercicioR, Tecnologico
from django.views import View
#from braces.views import JSONResponseMixin, LoginRequiredMixin
from django.contrib import messages
from datetime import datetime
# Create your views here.
#NOTAAA: Usuario lo marca como error, pero no es que esté mal, el plugin de python no lo reconoce que sea un comando
def principal(request):
    #aquí validamos que exista una sesión para que el usuario pueda entrar a la página principal
    if 'member_id' in request.session:
        errores = []
        try: 
            Aprendizaje.objects.get(IdUsuario_id = request.session['member_id'])
            return render(request,'principal.html')
        except Aprendizaje.DoesNotExist:
            errores.append('Debe completar este cuestionario primero')
            return render(request,'TestAprendizaje.html',{'errores':errores}) 

    return HttpResponseRedirect('/login/')
#vista para cerrar sesión
def logOut(request):
    try:
        del request.session['member_id']
    except KeyError:
        pass
    return HttpResponseRedirect('/principal/')

#vista para logear un usuario
def login(request):
    #lista para almacenar posibles errores
    errores = []
    if 'member_id' in request.session:
        return HttpResponseRedirect('/principal/')
    if request.method == 'POST':
        if not request.POST.get('usuario',''):
            errores.append('Introduce tu nombre de usuario')
        if not request.POST.get('password',''):
            errores.append('Introduce tu contraseña')
        if not errores:
            try:
                #obtenemos los datos del usuario desde la BD
                validar = Usuario.objects.get(NombreUsuario = request.POST['usuario'])
            except Usuario.DoesNotExist:
                errores.append('El nombre de usuario no existe')
            else:
                if validar.Password == request.POST['password']:
                    #request.session es un diccionario de datos, en el cual llenamos con información del usuario
                    request.session['member_id'] = validar.id
                    request.session['member_Nivel'] = validar.Nivel
                    request.session['member_Progreso'] = validar.Progreso
                    if validar.Nivel == 0 and validar.Progreso == 0:
                        #si el nivel del usuario y su progreso es 0, se le mandará al test de aprendizaje para saber 
                        #cómo aprende mejor las cosas
                        return HttpResponseRedirect('/TestAprendizaje/')
                        #si el usuario ya tiene un avance, se le manda a la página principal
                    return HttpResponseRedirect('/principal/')
                errores.append('La contraseña no es correcta')
    #si el usuario intenta ir al login, se valida si la sesión está activa, si es así, se le manda a la página principal

    return render(request,'login.html',{'errores':errores})

#vista para procesar el registro de un usuario
def registro(request):
    #lista para almacenar los errores
    errores=[]
    #aquí se debe colocar la condición para que si una sesión está activa, se impida el acceso al registro
    #el formulario envía los datos mediante POST
    if request.method == 'POST':
        #vamos a validar que el formulario esté completo, a pesar que ya se limitó el formulario en html, se verifica de nuevo
        if not request.POST.get('Nombre',''):
            errores.append('Por favor introduce tu nombre')
        if not request.POST.get('ApellidoP',''):
            errores.append('Por favor introduce tu apellido paterno')    
        if not request.POST.get('ApellidoM',''):
            errores.append('Por favor introduce tu apellido Materno')
        if not request.POST.get('Year',''):
            errores.append('Por favor introduce tu año de nacimiento')
        if not request.POST.get('Mes',''):
            errores.append('Por facor introduce tu mes de nacimiento')
        if not request.POST.get('Dia',''):
            errores.append('Por favor introduce tu día de nacimiento')
        if not request.POST.get('NUsuario',''):
            errores.append('Por favor introduce tu nombre de usuario')
        if not request.POST.get('Password',''):
            errores.append('Por favor introduce tu contraseña')
        if not request.POST.get('Password2',''):
            errores.append('Por favor introduce tu contraseña 2 veces')
        if not request.POST.get('Email',''):
            errores.append('Por favor introduce tu email')
        if not request.POST.get('Clave',''):
            errores.append('Por favor introduce la clave de tu tecnológico')
    #Si no hay errores, procedemos a validar que las contraseñas en los dos campos sean iguales
    #
    #
    #Hace falta saber si la clave de tecnológico se encuentra en la BD de la plataforma
    #
    #
        if not errores:
            if request.POST['Password'] == request.POST['Password2']:
                #ahora tenemos que verificar que no exista el Usuario que se va a registrar en la BD
                try:
                    Usuario.objects.get(NombreUsuario=request.POST['NUsuario'])
                except Usuario.DoesNotExist:
                    #aunque el usuario no exista, se debe tener cuidado que el correo tampoco se repita con algún otro
                    try:
                        Usuario.objects.get(Email = request.POST['Email'])
                    except Usuario.DoesNotExist:
                        #concateno para que se guarde la fecha de nacimiento de forma correcta
                        Fecha_Nacimiento = request.POST['Year'] + "-" + request.POST['Mes'] + "-" + request.POST['Dia']
                        #tuve que poner objects porque objec no jala xdxd
                        try:
                            idTec = Tecnologico.objects.get(ClavePlantel = request.POST['Clave'])
                        except Tecnologico.DoesNotExist:
                            errores.append('La clave de tu Tecnológico no está registrada, acude con tus profesores')
                            return render(request,'registro.html',{'errores':errores})

                        insert= Usuario(NombreUsuario=request.POST['NUsuario'],Nombre=request.POST['Nombre'],ApellidoPaterno=request.POST['ApellidoP'], ApellidoMaterno=request.POST['ApellidoM'],Password=request.POST['Password'],Email=request.POST['Email'],FechaNacimiento=Fecha_Nacimiento,Nivel=0, Progreso=0,IdTecnologico=idTec)
                        #Guardar en la BD
                        insert.save()
                        #retornar al login
                        return HttpResponseRedirect('/login/')
                    #ahora retrocediendo, si el email existe, almacena otro mensaje de error y así con el usuario
                    else:
                        errores.append('El Email ya existe')
                else:
                    errores.append('El usuario ya existe')
            if request.POST['Password'] != request.POST['Password2']:
                errores.append('Las contraseñas no coinciden')
#nos redirecciona al formulario de registro y devolvemos los errores
    return render(request,'registro.html',{'errores':errores})

def TestAprendizaje (request):
    errores = []
    if 'member_id' in request.session:
        if request.session['member_Nivel'] > 0 and request.session['member_Progreso'] > 0:
            return HttpResponseRedirect('/principal/')
        
        if request.method == 'POST':
            contador = 1       
            while contador < 19:                
                cont = str(contador)
                if not request.POST.get('p'+cont, False):
                    errores.append('Por favor contesta la pregunta: '+ cont)
                    return render(request,'TestAprendizaje.html',{'errores':errores}) 
                contador = contador + 1                     
                
            #comprobar que los campos estén completos
            #obtener los valores del formulario del test, organizados por Pregunta auditiva, visual y kinestésica
            Pa=int(request.POST['p2'])+int(request.POST['p5'])+int(request.POST['p12'])+int(request.POST['p14'])+int(request.POST['p15'])+int(request.POST['p17'])
            Pv=int(request.POST['p1'])+int(request.POST['p3'])+int(request.POST['p6'])+int(request.POST['p9'])+int(request.POST['p10'])+int(request.POST['p11'])
            Pk=int(request.POST['p4'])+int(request.POST['p7'])+int(request.POST['p8'])+int(request.POST['p13'])+int(request.POST['p16'])+int(request.POST['p18'])
            Pt= Pa+Pv+Pk
            #porcentaje de cada tipo de aprendizaje
            EvAu = round((Pa/Pt)*100,4)
            EvVi = round((Pv/Pt)*100,4)
            EvKi = round((Pk/Pt)*100,4)   
            #asignar la cantidad de ejercicios a mostrar al usuario         
            if EvAu > EvVi and EvAu > EvKi:
                CantidadAudi = 3
                CantidadVis = 1 
                CantidadKin = 1   
            if EvVi > EvAu and EvVi > EvKi:
                CantidadVis = 3
                CantidadAudi = 1
                CantidadKin = 1
            if EvKi > EvAu and EvKi > EvVi:
                CantidadKin = 3
                CantidadAudi = 1
                CantidadVis = 1
                #si las respuestas son todas iguales
            if EvKi == EvAu and EvAu == EvVi:
                CantidadKin = 2
                CantidadAudi = 1
                CantidadVis = 2              
            #guardar la información obtenida
            try:
                Aprendizaje.objects.get(IdUsuario_id = request.session['member_id'])
            except Aprendizaje.DoesNotExist:
                insert= Aprendizaje(Visual=EvVi,Auditivo=EvAu,Kinestesico=EvKi, CantidadVisuales = CantidadVis, CantidadAuditivos = CantidadAudi,CantidadKinestesicos = CantidadKin ,IdUsuario_id=request.session['member_id'])
                insert.save()
                #modificar el nivel y progreso del usuario
                Usuario.objects.filter(id=request.session['member_id']).update(Progreso=1)
                Usuario.objects.filter(id=request.session['member_id']).update(Nivel=1)
                #actualizar el diccionario de la sesión
                request.session['member_Nivel'] = 1
                request.session['member_Progreso'] = 1 
                return render(request, 'Resultados.html',{'Auditivo':EvAu, 'Visual':EvVi,'Kinestesico':EvKi})
            else:
                return HttpResponse("Consulta con el Administrador")               
                #si hay algun problema, vuelve al test
        return render(request,'TestAprendizaje.html',{'errores':errores})        
        #si no hay una sesion retorna al login
    return HttpResponseRedirect('/login/')

def tema(request, tema, subtema, pagina):
    #al retroceder después de cerrar sesión, la página carga, pero al recargar, si dirige correctamente
    #esto no pasa al iniciar sesión, y cerrar desde la página principal
    if 'member_id' in request.session:
         return render(request, 'Temas/%d-%d-%d.html'%(int(tema), int(subtema), int(pagina)))
    return HttpResponseRedirect('/principal/')

def EjercicioEj(request, clave, subtema, tipo, numero):
    if 'member_id' in request.session:
        return render(request,'Ejercicios/%s-%d-%s-%d.html'%(clave, int(subtema), tipo, int(numero)))
    return HttpResponseRedirect('/principal/')    

def Ejercicios(request, clave):
    if 'member_id' in request.session:
        if clave == "E1":
            tema = 1
        if clave == "E2":
            tema = 2
        if clave == "E3":
            tema = 3
        if clave == "E4":
            tema = 4
        if clave == "E5":
            tema = 5
        try:
            Aprendizaje.objects.get(IdUsuario_id = request.session['member_id'])
            try:
                Ejercicio.objects.filter(IdUsuario_id = request.session['member_id'])
                #if Ejercicio:
                #obtener la cantidad de ejercicios que debe hacer el usuario
                #getattr uff
                aprendi = Aprendizaje.objects.get(IdUsuario_id = request.session['member_id'])
                visuales = getattr(aprendi, 'CantidadVisuales')
                auditivos = getattr(aprendi, 'CantidadAuditivos')
               
                cantidad_resueltos_V = Ejercicio.objects.filter(Tipo = "V", IdTem = tema, IdUsuario_id = request.session['member_id']).count()
                cantidad_resueltos_A = Ejercicio.objects.filter(Tipo = "A", IdTem = tema, IdUsuario_id = request.session['member_id']).count()
                cantidad_resueltos_K = Ejercicio.objects.filter(Tipo = "K", IdTem = tema, IdUsuario_id = request.session['member_id']).count()
                cantidad_ejercicios = cantidad_resueltos_V + cantidad_resueltos_A + cantidad_resueltos_K
                subtema = random.randint(1,4)
                tipo_ejercicio = "V"
                #falta saber cuántos ejercicios de cada tipo existen para mostrar
                numero = 1 #random.randint(1,2)
                if cantidad_ejercicios < 5:
                    if cantidad_resueltos_V < visuales:
                        tipo_ejercicio = "V"
                    elif cantidad_resueltos_V >= visuales:
                        tipo_ejercicio = "A"
                    elif cantidad_resueltos_A >= auditivos:
                        tipo_ejercicio = "K"

                nombre_ejercicio = clave +"-"+str(subtema)+"-"+str(tipo_ejercicio)+"-"+str(numero)                                
                    #por aquí iría la verificación si el ejercicio fue resuelto con calf > 70

                while True: 
                    try:
                        if cantidad_ejercicios < 5:
                            if cantidad_resueltos_V < visuales:
                                tipo_ejercicio = "V"
                            elif cantidad_resueltos_V >= visuales:
                                tipo_ejercicio = "A"
                            elif cantidad_resueltos_A >= auditivos:
                                tipo_ejercicio = "K"
                        subtema = random.randint(1,4)                
                        numero = 1 #random.randint(1,2)
                        nombre_ejercicio = clave +"-"+str(subtema)+"-"+str(tipo_ejercicio)+"-"+str(numero)
                        Ejercicio.objects.get(Nombre = nombre_ejercicio, IdTem = tema, IdUsuario_id = request.session['member_id'])
                    except Ejercicio.DoesNotExist: 
                        return render(request,'Ejercicios/%s-%d-%s-%d.html'%(clave, subtema, tipo_ejercicio, numero))
                else:
                    #aquí debe mandar al siguiente tema
                    return HttpResponseRedirect('/principal/')
            except Ejercicio.DoesNotExist:
                #va a dirigir a un ejercicio visial por defecto
                sub = random.randint(1,4)
                num = random.randint(1,2)
                #esta condición es porque no hay una cantidad pareja de ejercicios de cada tipo
                if sub >= 2 and num >= 2:
                    num = 1
                return render(request,'Ejercicios/%s-%d-%s-%d.html'%(clave, sub, "V", num))                
        except Aprendizaje.DoesNotExist:
            return HttpResponseRedirect('/principal/')  

    return HttpResponseRedirect('/principal/')    

def evaluar (request, clave, subtema, tipo, numero):
    if 'member_id' in request.session:
        errores = []
        nombre_ejercicio = clave +"-"+str(subtema)+"-"+str(tipo)+"-"+str(numero) 
        try: 
            Ejercicio.objects.get(Nombre = nombre_ejercicio ,IdUsuario_id = request.session['member_id'])
            errores.append('Continue con el siguiente ejercicio')
            return render(request, 'respuestas.html',{'errores':errores, 'tema':clave})
        except Ejercicio.DoesNotExist:
            if clave == "E1":
                tema = 1
            if clave == "E2":
                tema = 2
            if clave == "E3":
                tema = 3
            if clave == "E4":
                tema = 4
            if clave == "E5":
                tema = 5
        
            if request.method == 'POST':
               
                respuestas = []
                contador = 1
                
                while True:
                    cont = str(contador)
                    if not request.POST.get('respuesta'+cont,''):
                        break
                    if request.POST['respuesta'+cont]:
                        respuestas.append(request.POST['respuesta'+cont])
                        contador +=1
                    else:
                        break
                tamaño = contador-1
                calificacion = 0
                cal_por_respuesta = 100 / tamaño
                #sacar la info de los ejercicios resueltos
                try:
                    respuestasBD = EjercicioR.objects.get(IdTem_id = tema, IdSub_id = subtema, Tipo = tipo, NEjer = numero)
                    contador = 1
                    for resp in respuestas:
                        cont = str(contador)
                        solucion = getattr(respuestasBD, 'RP'+cont)
                        if solucion == resp:
                            calificacion = calificacion + cal_por_respuesta
                        contador += 1
                
                    if calificacion >= 70:
                        if tamaño < 6:
                            respuestas.append('-')

                        insertar = Ejercicio(Nombre = clave+'-'+str(subtema)+'-'+tipo+'-'+str(numero), Tipo = tipo,
                        Evaluacion = round(calificacion),  Fecha = datetime.now(), R1 = respuestas[0], R2 = respuestas[1],
                        R3 = respuestas[2], R4 = respuestas[3], R5 = respuestas[4], R6 = respuestas[5],
                        IdSubtema_id = subtema, IdUsuario_id = request.session['member_id'], IdTem = tema)
                        insertar.save()
                        errores.append('Tu calificación fue de: '+str(calificacion))
                    else:
                        errores.append('No aprobaste el ejercicio, tu calificación fue de: '+str(calificacion))


                except EjercicioR.DoesNotExist:
                    errores.append("No se encontró el ejercicio")
            return render(request, 'respuestas.html',{'errores':errores, 'tema':clave})

    else:
        return HttpResponseRedirect('/principal/')
