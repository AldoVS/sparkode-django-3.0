"""sparkode URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from pnl.views import principal, login, registro, logOut, TestAprendizaje, tema, EjercicioEj, Ejercicios, evaluar
urlpatterns = [
    path('admin/', admin.site.urls),
    path('principal/', principal),
    path('login/', login),
    path('', login),
    path('registro/', registro),
    path('logOut/', logOut),
    path('TestAprendizaje/', TestAprendizaje),
    path('tema/<int:tema>/<int:subtema>/<int:pagina>', tema),
    path('ejercicioEj/<str:clave>/<int:subtema>/<str:tipo>/<int:numero>', EjercicioEj),
    path('ejercicio/<str:clave>',Ejercicios),
    path('evaluar/<str:clave>/<int:subtema>/<str:tipo>/<int:numero>', evaluar),
]
